/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aleatorio;

/**
 *
 * @author yazmin
 */
public class Guardar {
    public static int [] contagiosPorDia;
    public static int [] infectadosPorDia;
    public static int [] decesosPorDia;
    public static int [] recuperadosPorDia;
    
    public Guardar(int dias){
        infectadosPorDia = new int[dias];
        decesosPorDia = new int[dias];
        recuperadosPorDia = new int[dias];
    }
    
    public static void setInfectadosPorDia(int posicion, int dato){
        infectadosPorDia[posicion]=dato;
     }
    
    public static void setDecesosPorDia(int posicion, int dato){
        decesosPorDia[posicion]=dato;
     }
    
    public static void setRecuperadosPorDia(int posicion, int dato){
        recuperadosPorDia[posicion]= dato;
    }
    
    public static int getInfectadosPorDia(int dia){
        return  infectadosPorDia[dia];
    }
    public static int[] getDecesosPorDia(){
        return decesosPorDia;
    }
    public static int[] getRecuperadosPorDia(){
        return recuperadosPorDia;
    }

    public Guardar() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
